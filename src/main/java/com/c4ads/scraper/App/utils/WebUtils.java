package com.c4ads.scraper.App.utils;

public class WebUtils {

    public final static String BASE_URL = "https://c4ads.org/";

    public final static String REPORTS_URL = "https://c4ads.org/reports";

    public final static String CHAPTER_CLASS_NAME = ".sqs-block-button-element--small";

    public final static String ANCHOR_TAG = "a";

    public final static String HREF_ATTR = "href";

    public final static String RELATIVE_URL = "s/";

    public final static String DOWNLOAD_TEXT = "download";

    public final static String READ_MORE_TEXT = "read more";

}
