package com.c4ads.scraper.App.utils;

public class DirectoryPathUtils {

    public final static String DOWNLOADED_CHAPTERS = "download.json";

    public final static String CHAPTERS_PARAM = "chapters";

    public final static String DOWNLOAD_DIRECTORY = "C4ADS/";

}
