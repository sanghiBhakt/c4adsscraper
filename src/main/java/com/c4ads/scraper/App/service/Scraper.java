package com.c4ads.scraper.App.service;

import com.c4ads.scraper.App.utils.DirectoryPathUtils;
import com.c4ads.scraper.App.utils.WebUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.jsoup.Jsoup.connect;

public class Scraper {

    private static int secondsToSleep = 2;

    public boolean downloadChapterService() throws IOException, ParseException, InterruptedException {

        Document reportsDoc = connect(WebUtils.REPORTS_URL).get();
        Elements chapterElements = reportsDoc.select(WebUtils.ANCHOR_TAG + WebUtils.CHAPTER_CLASS_NAME);
        List<String> chapterUrls = new ArrayList<>();

        for(Element chapterElement: chapterElements) {
            if(chapterElement.text().equalsIgnoreCase(WebUtils.DOWNLOAD_TEXT) ||
                    chapterElement.text().equalsIgnoreCase(WebUtils.READ_MORE_TEXT)) {
                chapterUrls.add(chapterElement.attr(WebUtils.HREF_ATTR));
            }
        }

        List<String> downloadedChapters = readDownloadedChapters();
        List<String> toBeDownloadedChapters = new ArrayList<>();
        for(String chapterUrl: chapterUrls) {
            boolean alreadyDownloaded = false;
            String c4adsChapter = null;
            Pattern p = Pattern.compile("/(.*)/");
            Matcher m = p.matcher(chapterUrl);
            boolean isTinyUrl = true;
            if(m.find()) {
                if(m.group().equalsIgnoreCase("/s/")) {
                    isTinyUrl = true;
                }
                else {
                    isTinyUrl = false;
                }
            }
            p = Pattern.compile("([^/]+$)");
            m = p.matcher(chapterUrl);
            if(m.find()) {
                c4adsChapter = m.group();
            }
            for(String downloadedChapter: downloadedChapters) {
                try {
                    Integer.parseInt(c4adsChapter);
                } catch (NumberFormatException e) {
                    if(c4adsChapter.equals(downloadedChapter)) {
                        alreadyDownloaded = true;
                    }
                    continue;
                }
                alreadyDownloaded = true;
            }
            if(alreadyDownloaded == false) {
                if(isTinyUrl == true) {
                    System.out.println("[PASS] ToBeDownloadedChapter: " + c4adsChapter);
                    toBeDownloadedChapters.add(c4adsChapter);
                } else {
                    System.out.println("[PASS] ToBeDownloadedChapter: " + c4adsChapter);
                    toBeDownloadedChapters.add(chapterUrl);
                }
            }
        }

        if(toBeDownloadedChapters.isEmpty()) {
            return true;
        }

        List<String> justDownloadedChapters = downloadChapters(toBeDownloadedChapters);
        writeDownloadedChapter(justDownloadedChapters);
        return false;

    }

    public List<String> downloadChapters(List<String> toBeDownloadedChapters) throws IOException, InterruptedException {
        List<String> justDownloadedChapters = new ArrayList<>();

        for(String toBeDownloadedChapter: toBeDownloadedChapters) {
            URL c4adsChapterUrl = new URL(WebUtils.BASE_URL + WebUtils.RELATIVE_URL + toBeDownloadedChapter);
            getChapter(c4adsChapterUrl, toBeDownloadedChapter, justDownloadedChapters, 0);
        }
        return justDownloadedChapters;
    }

    public void getChapter(URL c4adsChapterUrl, String toBeDownloadedChapter, List<String> justDownloadedChapters, int counter) throws InterruptedException {

        if(counter >= 2) {
            return;
        }

        Thread.sleep(secondsToSleep * 1000);
        byte[] byteArray = new byte[1024];
        int bytesLength;
        try {
            System.out.println(c4adsChapterUrl.toString());
            URLConnection pdfUrl = c4adsChapterUrl.openConnection();
            if(!pdfUrl.getContentType().equalsIgnoreCase("application/pdf")) {
                System.out.println("[ERROR] Sorry:( This is not a PDF!");

                System.out.println("[INFO] Trying alternate url format!");
                Pattern p = Pattern.compile("([^/]+$)");
                Matcher m = p.matcher(toBeDownloadedChapter);
                if(m.find()) {
                    getChapter(new URL(toBeDownloadedChapter), m.group(), justDownloadedChapters, counter + 1);
                }
            } else {
                FileOutputStream fos = new FileOutputStream(DirectoryPathUtils.DOWNLOAD_DIRECTORY + toBeDownloadedChapter);
                try {
                    InputStream inputStream = c4adsChapterUrl.openStream();
                    while((bytesLength = inputStream.read(byteArray)) != -1) {
                        fos.write(byteArray, 0, bytesLength);
                    }
                    System.out.println("[PASS] Successfully Downloaded PDF!");
                    justDownloadedChapters.add(toBeDownloadedChapter);
                    fos.flush();
                    fos.close();
                    inputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            System.out.println("[Exception] IOException:(");
            e.printStackTrace();
        }
        return;

    }

    public void writeDownloadedChapter(List<String> downloadedChapters) throws IOException, ParseException {

        JSONParser jsonParser = new JSONParser();
        FileReader fileReader = new FileReader(DirectoryPathUtils.DOWNLOADED_CHAPTERS);

        Object object = jsonParser.parse(fileReader);
        JSONObject c4adsJson = (JSONObject) object;
        JSONArray chapters = (JSONArray) c4adsJson.get(DirectoryPathUtils.CHAPTERS_PARAM);

        for(String downloadedChapter: downloadedChapters) {
            chapters.add(downloadedChapter);
        }

        c4adsJson.put(DirectoryPathUtils.CHAPTERS_PARAM, chapters);
        FileWriter fileWriter = new FileWriter(DirectoryPathUtils.DOWNLOADED_CHAPTERS);
        fileWriter.write(c4adsJson.toJSONString());
        fileWriter.flush();

    }

    public List<String> readDownloadedChapters() throws IOException, ParseException {

        List<String> downloadedChapters = new ArrayList<>();
        JSONParser jsonParser = new JSONParser();
        FileReader fileReader = new FileReader(DirectoryPathUtils.DOWNLOADED_CHAPTERS);

        Object object = jsonParser.parse(fileReader);
        JSONObject c4adsJson = (JSONObject) object;
        JSONArray chapters = (JSONArray) c4adsJson.get(DirectoryPathUtils.CHAPTERS_PARAM);
        Iterator<String> iterator = chapters.iterator();
        while(iterator.hasNext()) {
            downloadedChapters.add(iterator.next());
        }
        System.out.println("[PASS] Successfully Read Downloaded Chapters!");
        return downloadedChapters;

    }

}
