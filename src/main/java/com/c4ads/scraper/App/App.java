package com.c4ads.scraper.App;

import com.c4ads.scraper.App.service.Scraper;
import org.json.simple.parser.ParseException;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

@SpringBootApplication
public class App {

	public static void main(String[] args) throws IOException, ParseException, InterruptedException {
	    System.out.println("[START] =================================================== ");
	    Scraper c4adsScraper = new Scraper();
	    boolean allChaptersDownloaded = false;
	    while(allChaptersDownloaded == false) {
            allChaptersDownloaded = c4adsScraper.downloadChapterService();
        }
        System.out.println("[DONE] =========================================== ");
	}

}
